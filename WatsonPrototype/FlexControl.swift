//
//  flexControl.swift
//  WatsonPrototype
//
//  Created by Dev Team on 25/10/2016.
//  Copyright © 2016 Empiricom. All rights reserved.
//

import UIKit


class flexControl: UIView {

    private var yesNoButtons = false
    private var microphoneImageView = UIImageView()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func YesButtonTapped() {
        print("Button tapped")
        self.backgroundColor = .blue
        
    }
    
    func NoButtonTapped() {
        print("Button tapped")
        self.backgroundColor = .green
    }
    
    
    
    func SetUpLayout(itype: InputLayoutType)
    {
        if(itype == InputLayoutType.YesNo)
        {
            //set up default
            let yesButton = UIButton(frame: CGRect(x: 50, y: 0, width: 120, height: 60))
            yesButton.setTitle("Yes", for: .normal)
            yesButton.backgroundColor = .gray
            
            //
            yesButton.addTarget(self, action: #selector(YesButtonTapped), for: .touchUpInside)
            
            
            self.addSubview(yesButton)
            
            let noButton = UIButton(frame: CGRect(x: 200, y: 0, width: 120, height: 60))
            noButton.setTitle("No", for: .normal)
            noButton.backgroundColor = .gray
            
            //
            noButton.addTarget(self, action: #selector(NoButtonTapped), for: .touchUpInside)
            
            
            self.addSubview(noButton)
            
            //add tap event
        }
        else if(itype == InputLayoutType.YesNoDontKnow)
        {
            //set up default
            let yesButton = UIButton(frame: CGRect(x: 30, y: 0, width: 100, height: 40))
            yesButton.setTitle("Yes", for: .normal)
            yesButton.backgroundColor = .gray
            self.addSubview(yesButton)
            
            let noButton = UIButton(frame: CGRect(x: 140, y: 0, width: 100, height: 40))
            noButton.setTitle("No", for: .normal)
            noButton.backgroundColor = .gray
            self.addSubview(noButton)
            
            let dontknowButton = UIButton(frame: CGRect(x: 250, y: 0, width: 100, height: 40))
            dontknowButton.setTitle("Don't Know", for: .normal)
            dontknowButton.backgroundColor = .gray
            self.addSubview(dontknowButton)
            
        }
        else if(itype == InputLayoutType.Keyboard)
        {
        
        }
        else if(itype == InputLayoutType.Audio)
        {
            
            // Show Microphone
            let imageName = "microphone.png"
            let microphoneImage = UIImage(named: imageName)
            microphoneImageView = UIImageView(image: microphoneImage)
            
            microphoneImageView.frame = CGRect(x: 180, y: 0, width: 40, height: 60)
            self.addSubview(microphoneImageView)
            
        }
        
    }

    

    
    
    
    
}
