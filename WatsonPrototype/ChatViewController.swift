//
//  ChatViewController.swift
//  WatsonPrototype
//
//  Created by Dev Team on 06/10/2016.
//  Copyright © 2016 Empiricom. All rights reserved.
//

import Foundation

import UIKit
import JSQMessagesViewController

class ChatViewController: JSQMessagesViewController {
    
    var messages = [JSQMessage]()
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    
    override func viewDidLoad() {
        
        self.senderId = "brb"
        self.senderDisplayName = "" // 4
        
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //self.view.resignFirstResponder()
        
        //self.inputToolbar.isHidden = true
        
        setupBubbles()
        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        collectionView!.backgroundColor = UIColor.black
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    private func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(
            with: UIColor.jsq_messageBubbleBlue())
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(
            with: UIColor.jsq_messageBubbleLightGray())
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!,
                                 avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    func addMessage(id: String, text: String) {
        let message = JSQMessage(senderId: id, displayName: "", text: text)
        messages.append(message!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        
        
        if message.senderId == senderId {
            cell.textView!.textColor = UIColor.white
        } else {
            cell.textView!.textColor = UIColor.black
        }
        
        return cell
    }
    
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        addMessage(id: senderId, text: text)
        
        // 4
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        // 5
        finishSendingMessage()
        
        parseText(speechText: text)
    }
    

    
    
    func parseText(speechText: String)
    {
        
        //gym = 18
        
        let todoEndpoint: String = "https://gateway-a.watsonplatform.net/calls/text/TextGetCombinedData?apikey=286435c37940f6cd66cab2ea0e2cd066afffdf80&&text="+speechText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!+"&outputMode=json"
        
        
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod="GET"
        let session = URLSession.shared
        
        session.dataTask(with: urlRequest){
            data, response,
            err in
            
            let attributes = try! JSONSerialization.jsonObject(with: data!) as! [String: AnyObject]
            if(attributes.count > 1)
            {

                DispatchQueue.main.sync {
                    
                    let entitiesArray = attributes["entities"] as! Array<AnyObject>
                    let numOfEntities = entitiesArray.count
                    
                    var entitiesDisplayText = "Entities: "
                    
                    if(numOfEntities>0)
                    {
                        for i in 0...numOfEntities-1 {
                            
                            let entityItemScore = entitiesArray[i]["relevance"] as? String
                            let entityItemText = entitiesArray[i]["text"] as? String
                            
                            entitiesDisplayText += entityItemText! + "  relevance:" + entityItemScore! + "     "
                        }
                        
                        self.addMessage(id: "nil", text: entitiesDisplayText)
                        self.finishReceivingMessage()
                        
                    }
                    
                    
                    ///////////////
                    
                    let conceptsArray = attributes["concepts"] as! Array<AnyObject>
                    let numOfConcepts = conceptsArray.count
                    
                    var conceptDisplayText = "Concepts: "
                    
                    if(numOfConcepts>0)
                    {
                        for i in 0...numOfConcepts-1 {
                            
                            let conceptItemScore = conceptsArray[i]["relevance"] as? String
                            let conceptItemText = conceptsArray[i]["text"] as? String
                            
                            conceptDisplayText += conceptItemText! + "  relevance:" + conceptItemScore! + "     "
                        }
                        
                        //self.addMessage(id: "nil", text: conceptDisplayText)
                        //self.finishReceivingMessage()
                        
                    }

                    
                    
                    ///////////////
                    
                    let keywordsArray = attributes["keywords"] as! Array<AnyObject>
                    let numOfKeywords = keywordsArray.count
                    
                    var keywordDisplayText = "Keywords: "
                    
                    if(numOfKeywords>0)
                    {
                    for i in 0...numOfKeywords-1 {
                        
                        let keywordItemScore = keywordsArray[i]["relevance"] as? String
                        let keywordItemText = keywordsArray[i]["text"] as? String
                        
                        
                        keywordDisplayText += keywordItemText! + "  relevance:" + keywordItemScore! + "     "
                    }
                        
                    //self.addMessage(id: "nil", text: keywordDisplayText)
                    //self.finishReceivingMessage()
                            
                    }
                    
                    ///////////

                    
                    let categoryArray = attributes["taxonomy"] as! Array<AnyObject>
                    let numOfCategories = categoryArray.count
                    var categoryDisplayText = "Categories: "
                    
                    if(numOfCategories>0)
                    {
                    for i in 0...numOfCategories-1 {
                    
                        let categoryType = categoryArray[i]["label"] as? String
                        let categoryScore = categoryArray[i]["score"] as? String
                        categoryDisplayText += categoryType! + "  relevance:" + categoryScore! + "     "
                    }
                        
                    //self.addMessage(id: "nil", text: categoryDisplayText)
                    //self.finishReceivingMessage()
                        
                    }
                    
                    //**** Do more categories and format better
                    
                    //confirm with user that have understood correctly after checking graphs
                    
                    //lets say we have 4 condition graphs - Ear / Nose / Throat / Head / chest
                    
                    //each graph may share factors
                    
                    //each graph will have associated 'tags' e.g. 'sinus' 'snoring' for nose
                    
                    //each graph where there is a match on tags will be consulted until there is an overturn
                    
                    
                }
            }
            }.resume()
        
    }
    
}
