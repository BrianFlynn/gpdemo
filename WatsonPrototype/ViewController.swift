//
//  ViewController.swift
//  WatsonPrototype
//
//  Created by Dev Team on 06/10/2016.
//  Copyright © 2016 Empiricom. All rights reserved.
//

import UIKit
import Speech
import AVFoundation
import Foundation



class ViewController: UIViewController, SFSpeechRecognizerDelegate, AVSpeechSynthesizerDelegate, SFSpeechRecognitionTaskDelegate, URLSessionDataDelegate {
    
    
    @IBOutlet weak var newView: UIView!

    private var chatViewController : ChatViewController?
    private var inputNode: AVAudioInputNode!
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-GB"))
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    private let speechSynthesizer = AVSpeechSynthesizer()
    private var isFinished = false
    private var isConsultationStarted = false
    private var isInAudioMode = true
    
    private var newInputView = NewInputView(frame: CGRect(x: 0, y: 0, width: 375, height: 82))
    private var interimText = ""
    private var consultationId = 0
    private var lastIETextResponse = ""
    
    private var username = ""
    private var userGUID = "" //Must be stored on login - faked in the meantime
    private var defaults = UserDefaults.standard
    
    private var interimTranscription = ""
    private var timerStartedTranscription = ""
    private var EOSdetected=false
    
    private var haveEstablishedWhoConsultationFor = false
    
    private var gotInitialSymptoms = false
    
    
    
    private var interimResults = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        speechRecognizer?.delegate = self
        speechSynthesizer.delegate = self
        defaults.set("Brian", forKey: "Username")
        
        self.chatViewController?.inputToolbar.isHidden = true //unless free input mode as determined by response
        

        SFSpeechRecognizer.requestAuthorization{(authStatus) in
            
            switch authStatus{
            case .authorized: break
                //isButtonEnabled = true
            case .denied:
                //isButtonEnabled = false;
                print("User denied access to speech recognition")
            case .restricted:
                //isButtonEnabled = false;
                print("Speech recognition restricted on this device")
            case .notDetermined:
                //isButtonEnabled = false
                print("Speech recognition not yet authorised ")
            }
        }
        
        
        username = defaults.object(forKey:"Username") as! String
        
        let openingText = "Hello "+username+", who is the consultation for? You? Or someone else?"
        respondToUser(text: openingText)
        if(!isInAudioMode)
        {
            newInputView.SetUpLayout(itype: InputLayoutType.DetermineUser)
            newView.addSubview(newInputView)
        }
        else
        {
            newInputView.SetUpLayout(itype: InputLayoutType.Audio)
            newView.addSubview(newInputView)
        }
        
        }
    
    
    
    func respondToUser(text:String)
    {
        self.addChatMessage(forUser:false, chatMessage: text)
        if(isInAudioMode)
        {
            self.SpeakText(questionText: text)
        }
    }
    
    
    func addChatMessage(forUser: Bool,  chatMessage: String)
    {
        var id="nil"
        if(forUser)
        {
            id = "brb"
        }
        
        self.chatViewController?.addMessage(id: id, text: chatMessage)
        self.chatViewController?.finishReceivingMessage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            //microphone.isHidden = false
        } else {
            //microphone.isHidden = true
        }
    }
    
    
    func startRecording() {
        
        if(isInAudioMode)
        {
            EOSdetected = false
            if audioEngine.isRunning {
                audioEngine.stop()
                recognitionRequest?.endAudio()
            
                newInputView.HideMicrophone()
            } else {
                newInputView.ShowMicrophone()
            
                if recognitionTask != nil {
                recognitionTask?.cancel()
                recognitionTask = nil
                }
            
                let audioSession = AVAudioSession.sharedInstance()
                do {
                try audioSession.setCategory(AVAudioSessionCategoryRecord)
                try audioSession.setMode(AVAudioSessionModeMeasurement)
                try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
                } catch {
                print("audioSession properties weren't set because of an error.")
            }
            
            recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
            inputNode = audioEngine.inputNode!
            
            guard let recognitionRequest = recognitionRequest else {
                fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
            }
            
            let recordingFormat = inputNode.outputFormat(forBus: 0)
            inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
                self.recognitionRequest?.append(buffer)
            }
            
            audioEngine.prepare()
            
            do {
                try audioEngine.start()
            } catch {
                print("audioEngine couldn't start because of an error.")
            }
            
            recognitionRequest.shouldReportPartialResults = true
            recognitionTask = speechRecognizer?.recognitionTask(with: (recognitionRequest), delegate: self)
            
            }
        }
    }
    

    //if in the time period since the last interim result nothing has changed or there has just been that one result ten we have EOS
    func checkForEndOfSpeech() {

        //print ("Is Equal: " + String(timerStartedTranscription == interimTranscription))
        //print (timerStartedTranscription)
        //print (interimTranscription)
        
        if(!EOSdetected)
        {
            if((timerStartedTranscription == interimTranscription) || (interimResults == 1)) // or there has only been one interim result
            {
                EOSdetected = true
                interimResults = 0
                categoriseSpeechInput()
            }
            else{
                timerStartedTranscription = interimTranscription
            }
        }
    }
    
    func categoriseSpeechInput()
    {
        self.addChatMessage(forUser: true, chatMessage: interimTranscription)
        var voiceInputType = UserInputType.Undefined
        voiceInputType = self.processVoiceInput(voiceInput: interimTranscription)
        
        //have a separate clause for the 'eliciting symptoms' phase?
        
        
        if(isConsultationStarted)
        {
            
            switch voiceInputType {
            case .True :
                //call IE and pass true
                SendInferenceResponse(stringResponse: "1")
                self.EndRecording()
            case .False :
                SendInferenceResponse(stringResponse: "0")
                self.EndRecording()
            case .DontKnow :
                SendInferenceResponse(stringResponse: "2") //can inference engine handle unknown response?
                self.EndRecording()
            case .Repeat :
                self.EndRecording()
                SpeakText(questionText: lastIETextResponse) //get last ios response
            case .GoBack :
                self.EndRecording()
                //SpeakText(questionText: lastIETextResponse, addToChat: false) //get last ios response
                //Inference Move Back Up Stack
                
            case .NonBoolResponse : break
            //
            case .Undefined : break
                //carry on recording
                
            default:
                break
            }
        }
        else //pre-consultation
        {
            if(!haveEstablishedWhoConsultationFor)
            {
                switch voiceInputType {
                case .Me :
                    //StartConsultationMock(kbid: 7)
                    GetUsersSymptomDescription()
                    self.EndRecording()
                case .SomeoneElse :
                    //StartConsultationMock(kbid: 7)
                    GetUsersSymptomDescription()
                    self.EndRecording()
                case .Undefined : break //ask user to repeat
                    
                default:
                    break
                }
            }
            else //eliciting symptoms
            {
                //*** either has matched a graphs metadata or hasnt (ask user to repeat..)
               self.EndRecording()
               switch voiceInputType {
//                case .Me :
//                    //StartConsultationMock(kbid: 7)
//                    GetUsersSymptomDescription()
//                    self.EndRecording()
//                case .SomeoneElse :
//                    //StartConsultationMock(kbid: 7)
//                    GetUsersSymptomDescription()
//                    self.EndRecording()
//                case .Undefined : break //ask user to repeat
//                    
                default:
                    break
               }
            }
            
            
         }
    }
    
    //timer is used to hack End-Of-Speech detection is Siri is currently lacking it
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didHypothesizeTranscription transcription: SFTranscription) {
        
        var timer = Timer() //make global?
        
        if(transcription.formattedString != "")
        {
             timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(checkForEndOfSpeech), userInfo: nil, repeats: false)
             interimTranscription = transcription.formattedString
            interimResults += 1
        }
    }
    
    
    func EndRecording()
    {
        self.inputNode.removeTap(onBus: 0)
        
        newInputView.HideMicrophone()
        self.audioEngine.stop()
        
        self.recognitionTask?.cancel()
        self.recognitionRequest?.endAudio()
        
        self.recognitionRequest = nil
        self.recognitionTask = nil
    }
    
    
    func speechRecognitionDidDetectSpeech(_ task: SFSpeechRecognitionTask) {
        print("speechRecognitionDidDetectSpeech")
    }
    
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishSuccessfully: Bool)
    {
        let st = task.state
    }
    
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishRecognition recognitionResult: SFSpeechRecognitionResult) {

    }
    
    
    //app stopped speaking
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance)
    {
        if(!self.isFinished)
        {
            self.startRecording() // not if consultation finished
        }
    }
    
    
    
    func SpeakText (questionText: String){
        
        if(isInAudioMode)
        {
            let audioSession = AVAudioSession.sharedInstance()
            
            do {
                try audioSession.setCategory(AVAudioSessionCategoryPlayback)
                try audioSession.setMode(AVAudioSessionModeMeasurement)
                try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
            } catch {
                print("audioSession properties weren't set because of an error.")
            }
            
            
            let speechUtterance = AVSpeechUtterance(string: questionText)
            speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
            self.speechSynthesizer.speak(speechUtterance)
            
            audioEngine.stop()
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "chatSegue") {
            chatViewController = segue.destination as? ChatViewController
            // Now you have a pointer to the child view controller.
            // You can save it, or call public functions on it
        }
    }
    
    func processVoiceInput(voiceInput:String) -> UserInputType
    {
        let voiceInputUpper = voiceInput.uppercased()
        
        if(isInputTrue(voiceInput: voiceInputUpper))
        {
            return UserInputType.True
        }
        else if(isInputFalse(voiceInput: voiceInputUpper))
        {
            return UserInputType.False
        }
        else if(isInputDontKnow(voiceInput: voiceInputUpper))
        {
            return UserInputType.DontKnow
        }
        else if(isInputRepeat(voiceInput: voiceInputUpper))
        {
            return UserInputType.Repeat
        }
        else if(isInputMe(voiceInput: voiceInputUpper))
        {
            return UserInputType.Me
        }
        else if(isInputSomeoneElse(voiceInput: voiceInputUpper))
        {
            return UserInputType.SomeoneElse
        }
        else if(isRecognisable(voiceInput: voiceInputUpper))
        {
            return UserInputType.NonBoolResponse
        }
        else
        {
            return UserInputType.Undefined
        }
    }
    
    
    func isInputTrue(voiceInput:String)->Bool
    {
        if(voiceInput=="YES" || voiceInput=="YEAH" || voiceInput=="YEP")
        {
            return true
        }
        return false
    }
    
    func isInputFalse(voiceInput:String)->Bool
    {
        if(voiceInput=="NO" || voiceInput=="NOPE")
        {
            return true
        }
        return false
    }
    
    func isInputDontKnow(voiceInput:String)->Bool
    {
        if(voiceInput=="DON'T KNOW" || voiceInput=="NOT SURE") //or includes don't
        {
            return true
        }
        return false
    }
    
    func isInputRepeat(voiceInput:String)->Bool
    {
        if(voiceInput=="REPEAT")
        {
            return true
        }
        return false
    }
    
    func isInputMe(voiceInput:String)->Bool
    {
        if(voiceInput=="ME")
        {
            return true
        }
        return false
    }
    
    func isInputSomeoneElse(voiceInput:String)->Bool
    {
        if(voiceInput=="SOMEONE ELSE")
        {
            return true
        }
        return false
    }
    
    
    
    func isRecognisable(voiceInput:String)->Bool
    {
        //is this an expected phrase? does it match a graph's keyword?
        return true
    }
    
    func StartConsultationMock(kbid: Int)
    {
        let urlString = "http://31.193.7.82:56102/json/StartConsultation?userID=6&kbid=7&isTest=false"
        let backgroundConfigObject = URLSessionConfiguration.background(withIdentifier: "myIdentifierStart")
        let backgroundSession = URLSession(configuration: backgroundConfigObject, delegate: self, delegateQueue: nil)
        let retrieveTask = backgroundSession.dataTask(with: URL(string: urlString)!)
        retrieveTask.resume()
        
    }
    
    func SendInferenceResponse(stringResponse: String)
    {
        
        let consultationString = String(describing: self.consultationId)
        let urlString =  "http://31.193.7.82:56102/json/MoveNext?consultId="+consultationString+"&answer="+stringResponse+"&userName=brianedflynn@gmail.com"
        
        let backgroundConfigObject = URLSessionConfiguration.background(withIdentifier: "myIdentifierSendInferenceResponse")
        let backgroundSession = URLSession(configuration: backgroundConfigObject, delegate: self, delegateQueue: nil)
        let retrieveTask = backgroundSession.dataTask(with: URL(string: urlString)!)
        retrieveTask.resume()

        
    }
    
  
    
    
    //data returned
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        let attributes = try! JSONSerialization.jsonObject(with: data) as! [String: AnyObject]
        let responseType = attributes["ResponseType"]! as! String
        
        if(attributes.count > 1)
        {
            if responseType == "Question"
            {
                self.isConsultationStarted = true
                let questionText = attributes["QuestionText"]! as? String
                self.consultationId = (attributes["ConsultationId"]! as? Int)!
            
                DispatchQueue.main.sync {
                    respondToUser(text: questionText!)
                    lastIETextResponse = questionText!
                    
                    if(isInAudioMode)
                    {
                        newInputView.SetUpLayout(itype: InputLayoutType.Audio)
                    }
                    else{
                        newInputView.SetUpLayout(itype: InputLayoutType.YesNo)
                    }
                    newView.addSubview(newInputView)

                    
                }
            }
            else if responseType == "ReportSection"
            {
                self.isFinished = true
                let explanation = attributes["Explanation"]! as! Dictionary<String, AnyObject>
                let explanationDetails = explanation["AutoExplanation"] as! String
                
                DispatchQueue.main.sync {
                    respondToUser(text: explanationDetails)
                    if(self.isInAudioMode)
                    {
                        EndRecording()
                        lastIETextResponse = explanationDetails
                    }
                }
            }
        }
    }
    
    
    func YesButtonTapped() {
        newInputView.removeFromSuperview()
        SendInferenceResponse(stringResponse: "1")
        addChatMessage(forUser: true,chatMessage: "Yes")
    }
    
    func NoButtonTapped() {
        newInputView.removeFromSuperview()
        SendInferenceResponse(stringResponse: "0")
        addChatMessage(forUser: true,chatMessage: "No")
    }
    
    func DontKnowButtonTapped() {
        newInputView.removeFromSuperview()
        SendInferenceResponse(stringResponse: "2")
        addChatMessage(forUser: true,chatMessage: "Don't Know")
    }
    
    func MeButtonTapped() {
        newInputView.removeFromSuperview()
        addChatMessage(forUser: true,chatMessage: "Me")
        //StartConsultationMock(kbid: 7)
        
        GetUsersSymptomDescription()
        
        //get user id
    }
    
    func SomeoneElseButtonTapped() {
        newInputView.removeFromSuperview()
        addChatMessage(forUser: true,chatMessage: "Someone else")
        //StartConsultationMock(kbid: 7)
        
        GetUsersSymptomDescription()
        
        //set guest mode
    }
    
    func GetUsersSymptomDescription()
    {
        haveEstablishedWhoConsultationFor = true
        respondToUser(text: "OK. Can you give a short description of what the problem appears to be?")
    }
    
    

    
    
    
    
}






