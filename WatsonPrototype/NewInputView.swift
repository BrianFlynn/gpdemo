//
//  inputView.swift
//  WatsonPrototype
//
//  Created by Dev Team on 28/10/2016.
//  Copyright © 2016 Empiricom. All rights reserved.
//

import UIKit

class NewInputView: UIView {
    
    private var microphoneImageView = UIImageView()

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func ShowMicrophone()
    {
        microphoneImageView.isHidden = false
    }
    
    func HideMicrophone()
    {
        microphoneImageView.isHidden = true
    }
    
    
    
    func SetUpLayout(itype: InputLayoutType)
    {
        if(itype == InputLayoutType.YesNo)
        {
            //set up default
            let yesButton = UIButton(frame: CGRect(x: 50, y: 0, width: 120, height: 60))
            yesButton.setTitle("Yes", for: .normal)
            yesButton.backgroundColor = .gray
            yesButton.addTarget(nil, action: Selector(("YesButtonTapped")), for: .touchUpInside)
            self.addSubview(yesButton)
            
            let noButton = UIButton(frame: CGRect(x: 200, y: 0, width: 120, height: 60))
            noButton.setTitle("No", for: .normal)
            noButton.backgroundColor = .gray
            noButton.addTarget(nil, action: Selector(("NoButtonTapped")), for: .touchUpInside)
            self.addSubview(noButton)
        }
        if(itype == InputLayoutType.DetermineUser)
        {
            //set up default
            let meButton = UIButton(frame: CGRect(x: 50, y: 0, width: 120, height: 60))
            meButton.setTitle("Me", for: .normal)
            meButton.backgroundColor = .gray
            meButton.addTarget(nil, action: Selector(("MeButtonTapped")), for: .touchUpInside)
            self.addSubview(meButton)
            
            let someoneElseButton = UIButton(frame: CGRect(x: 200, y: 0, width: 120, height: 60))
            someoneElseButton.setTitle("Someone else", for: .normal)
            someoneElseButton.backgroundColor = .gray
            someoneElseButton.addTarget(nil, action: Selector(("SomeoneElseButtonTapped")), for: .touchUpInside)
            self.addSubview(someoneElseButton)
        }
            
        else if(itype == InputLayoutType.YesNoDontKnow)
        {
            //set up default
            let yesButton = UIButton(frame: CGRect(x: 30, y: 0, width: 100, height: 40))
            yesButton.setTitle("Yes", for: .normal)
            yesButton.backgroundColor = .gray
            yesButton.addTarget(nil, action: Selector(("YesButtonTapped")), for: .touchUpInside)
            self.addSubview(yesButton)

            let noButton = UIButton(frame: CGRect(x: 140, y: 0, width: 100, height: 40))
            noButton.setTitle("No", for: .normal)
            noButton.backgroundColor = .gray
            noButton.addTarget(nil, action: Selector(("NoButtonTapped")), for: .touchUpInside)
            self.addSubview(noButton)

            
            let dontknowButton = UIButton(frame: CGRect(x: 250, y: 0, width: 100, height: 40))
            dontknowButton.setTitle("Don't Know", for: .normal)
            dontknowButton.backgroundColor = .gray
            dontknowButton.addTarget(nil, action: Selector(("DontKnowButtonTapped")), for: .touchUpInside)
            self.addSubview(dontknowButton)
            
            //add tap event
        }
        else if(itype == InputLayoutType.Keyboard)
        {
            
        }
        else if(itype == InputLayoutType.Audio)
        {
            // Show Microphone
            let imageName = "microphone.png"
            let microphoneImage = UIImage(named: imageName)
            microphoneImageView = UIImageView(image: microphoneImage)
            
            microphoneImageView.frame = CGRect(x: 170, y: 0, width: 40, height: 60)
            microphoneImageView.isHidden = true;
            
            
            self.addSubview(microphoneImageView)
            
        }

    }

}
