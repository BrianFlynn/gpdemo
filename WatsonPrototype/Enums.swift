//
//  Enums.swift
//  WatsonPrototype
//
//  Created by Dev Team on 26/10/2016.
//  Copyright © 2016 Empiricom. All rights reserved.
//

import Foundation


enum InputLayoutType {
    case YesNo
    case YesNoDontKnow
    case DetermineUser
    case Audio
    case FactorGroup
    case Keyboard
}

enum UserInputType {
    case True
    case False
    case DontKnow
    case Repeat
    case GoBack
    case Me
    case SomeoneElse
    case NonBoolResponse
    case Undefined
}

    

